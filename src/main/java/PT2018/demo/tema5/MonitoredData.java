package PT2018.demo.tema5;


import java.util.Date;


public class MonitoredData {
	public MonitoredData(Date starttime, Date endtime, String activitylabel) {
		super();
		this.starttime = starttime;
		this.endtime = endtime;
		this.activitylabel = activitylabel;
	}
	Date starttime;
	Date endtime;
	String activitylabel;
	public Date getStarttime() {
		return starttime;
	}
	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}
	public Date getEndtime() {
		return endtime;
	}
	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}
	public String getActivitylabel() {
		return activitylabel;
	}
	public void setActivitylabel(String activitylabel) {
		this.activitylabel = activitylabel;
	}
	@Override
	public String toString() {
		return "MonitoredData [starttime=" + starttime + ", endtime=" + endtime + ", activitylabel=" + activitylabel
				+ "]";
	}
	public int insecunde() {
		int diferenta=0;
		int ore1 = (starttime.getHours())*3600;
		int minute1 =(starttime.getMinutes())*60;
		int ore2=(endtime.getHours())*3600;
		int minute2=(endtime.getMinutes())*60;
		diferenta = (ore2+minute2+endtime.getSeconds())-(ore1+minute1+starttime.getSeconds());
		return diferenta;
	}
	
}
