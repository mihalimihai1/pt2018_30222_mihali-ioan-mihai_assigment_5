

package PT2018.demo.tema5;
import static java.util.stream.Collectors.groupingBy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.text.DateFormatter;
/**
 * Hello world!
 *
 */
public class App 
{	
	public static List<MonitoredData> list = new ArrayList<>();
	FileWriter fstream;
	BufferedWriter out;
	static PrintWriter f1,f2,f3;
	
	 interface Adder{
		double adunare(double x, double y);
	}
	 public static  Map<Integer,Map<String,Long>> activitati(List<MonitoredData> list) {
		 Map<Integer, Map<String, Long>> ac = list.stream()
				 .collect(groupingBy(m->m.getStarttime().getDay(),
				 		  groupingBy(m->m.getActivitylabel(),Collectors.counting())));
		return ac;
	 }
	 
	 @SuppressWarnings("deprecation")
	public static void numar_zile(List<MonitoredData> list) {
		 
		long count = list.stream().map(m -> m.starttime.getDay()).distinct().count();
			System.out.println(2*count);
	 }
	 
	 public static Map<String,Long> activitati_diferite(List<MonitoredData> list) {
	        Map<String,Long> activ = list.stream().map(a -> a.getActivitylabel())
	        								.collect(groupingBy(Function.identity(),Collectors.counting()));
	        return activ;
	    }
	 
	
	 public static  Map<Integer,Map<String,Long>> activitati_pe_zile(List<MonitoredData> list, PrintWriter f2) {
		 Map<Integer, Map<String, Long>> ac = list.stream().collect(groupingBy(m->m.getStarttime().getDay(),
				 													groupingBy(m->m.getActivitylabel(),Collectors.counting())));
		return ac;
	 }
	 @SuppressWarnings("hiding")
	public static List<String> minute(List<MonitoredData> list){
		// Map<String, Long> m1=(Map<String, Long>) activitati_diferite(list);
	//	 Map<String, Long> m2=list.stream().filter(s->s.getEndtime())
	//			 						.map(a -> a.getActivitylabel())
	//				.collect(groupingBy(Function.identity(),Collectors.counting()));
	//	 List<String> m3 = (List<String>) list.stream();
		 Map<String,Long> a= activitati_diferite(list);
	        Map<String,Long> b= list.stream()
	        		                .filter(m->m.insecunde()<300)
	        		                .map(m->m.getActivitylabel())
	                                .collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
	        List <String> x=  (List<String>) b.entrySet().stream()
	        					 .filter(s->s.getValue()*100>=90*a.get(s.getKey()))
	        					 .map(m->m.getKey()).distinct().collect(Collectors.toList());
		 return x;
	 }
	 
	 
	 
    public static void main( String[] args ) throws IOException, ParseException
    {
    	//cream fisierele
    	f1 = new PrintWriter("D://PT2018/tema5/rezul1.txt");
    	f2= new PrintWriter("D://PT2018/tema5/rezul2.txt");
    	f3 = new PrintWriter("D://PT2018/tema5/rezult3.txt");
    	
    	DateFormat format = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
    	BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("D://PT2018/tema5/Activities.txt")));
    	String firstLine;
    	 while((firstLine=reader.readLine())!=null) {
    			String prima, secund;
    	    	//System.out.println("Read a line:" + firstLine);
    	    	StringTokenizer stk = new StringTokenizer(firstLine);
    	    	
    	    	prima = stk.nextToken()+" "+ stk.nextToken();
    	    	//System.out.println(prima);
    	    	Date a = format.parse(prima);
    	    	
    	    	secund = stk.nextToken() + " " + stk.nextToken();
    	    	//System.out.println(secund);
    	    	Date b = format.parse(secund);
    	    	
    	    	String c = stk.nextToken();
    	    	
    	    	MonitoredData  linie =  new MonitoredData(a, b, c);
    	    	list.add(linie);
    	    	
    	    	//System.out.println(linie.getStarttime());
    	    	//System.out.println(linie.getEndtime());
    	    	//System.out.println(linie.getActivitylabel());
    	    	
    	 }
    	reader.close();
    	
    	//Nr.zile
    	numar_zile(list);
    	activitati_diferite(list).forEach((index,val)->f1.println(index+": "+val));
    	activitati(list).forEach((index,val)->f2.println(index+": "+val));
    	activitati_pe_zile(list,f2).forEach((index,val)->f2.println((index+7)+": "+val));
    	minute(list).forEach(m->f3.println(m));
    	f1.close();f2.close();f3.close();
    	
    	Adder adder = (x,y) -> x+y;
    	double sum1 =  adder.adunare(1, 89.11); 
    	System.out.println("O adunare:"+sum1);
        
    }
}
